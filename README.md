# Check expiration and valaidity of domain registration and SSL cert

![pipeline status](https://gitlab.com/brie/working-as-intended/badges/main/pipeline.svg) | ![docker pulls](https://badgen.net/docker/pulls/brie/perl-whois-expiry)

This project represents the set of tools that I use to monitor the domain name expiration and digital certificate expiration information for a subset of my domain names. I rely on these tools:

  - [testssl.sh](https://github.com/drwetter/testssl.sh)
  - [Net::Domain::ExpireDate](https://metacpan.org/pod/Net::Domain::ExpireDate) - (Perl) - `Net::Domain::ExpireDate` gets WHOIS information of given domain using Net::Whois::Raw and tries to obtain expiration date of domain.

The resulting report is served via GitLab Pages and is available at [brie.gitlab.io/working-as-intended](https://brie.gitlab.io/working-as-intended).
